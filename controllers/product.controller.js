const asyncHandler = require('../middleware/async');
const Product = require('../models/product.model');
const User = require('../models/user.model');
const { mapRequestErrors } = require('../utils/helpers');


// @desc      Create a product
// @route     POST /api/v1/products
// @access    Public
exports.newProduct = asyncHandler(async (req, res) => {
    var error = mapRequestErrors(req);
    if (!error.success) {
        return res.status(400).send({
            success: false,
            error: error.error
        });
    }

    const product = await Product.create(req.body);
    res.send({
        success: true,
        product
    });
});


// @desc      Get all products
// @route     GET /api/v1/products
// @access    Public
exports.getProducts = asyncHandler(async (req, res) => {
    const products = await Product.find();
    res.send({
        success: true,
        products
    });
});


// @desc      Buy a product (sending a product id with quantity and user id)
// @route     GET /api/v1/products/buy
// @access    Public
exports.buyProduct = asyncHandler(async (req, res) => {
    var error = mapRequestErrors(req);
    if (!error.success) {
        return res.status(400).send({
            success: false,
            error: error.error
        });
    }

    const { userId, product } = req.body;
    await User.updateOne({ '_id': userId }, { $push: { products: { product: product.id, quantity: product.quantity } } });
    await Product.updateOne({ '_id': product.id }, { $inc: { stock: -1 * product.quantity } });
    res.send({
        success: true,
    });
});

// @desc      Get all products purchased by a user
// @route     GET /api/v1/products/me
// @access    Private
exports.userProducts = asyncHandler(async (req, res) => {
    const { products } = await User.findById(req.user.id).populate('products.product');
    res.send({
        success: true,
        products
    });
});