const asyncHandler = require('../middleware/async');
const User = require('../models/user.model');
const { sendTokenResponse, mapRequestErrors } = require('../utils/helpers');

// @desc      Signup user
// @route     POST /api/v1/auth/signup
// @access    Public
exports.signup = asyncHandler(async (req, res) => {
    var error = mapRequestErrors(req);
    if (!error.success) {
        return res.status(400).send({
            success: false,
            error: error.error
        });
    }

    // Create user & return token
    const user = await User.create(req.body);
    sendTokenResponse(user, 200, res);
});


// @desc      Login user
// @route     POST /api/v1/auth/login
// @access    Public
exports.login = asyncHandler(async (req, res) => {
    const { email, password } = req.body;

    var error = mapRequestErrors(req);
    if (!error.success) {
        return res.status(400).send({
            success: false,
            error: error.error
        });
    }

    // Check for user
    const user = await User.findOne({ email }).select('+password');
    if (!user) {
        return res.status(400).send({
            success: false,
            error: {
                message: "User you entered does not exist"
            }
        });
    }

    // Check if password matches
    const isMatch = await user.matchPassword(password);
    if (!isMatch) {
        return res.status(400).send({
            success: false,
            error: {
                message: "Invalid credentials"
            }
        });
    }

    sendTokenResponse(user, 200, res);
});