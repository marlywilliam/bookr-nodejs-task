const { validationResult } = require('express-validator');

// Create token Response
exports.sendTokenResponse = (user, statusCode, res) => {
    const token = user.getSignedJwtToken();
    const { email, firstname, lastname, _id } = user;
    res.status(statusCode).json({
        success: true,
        token,
        user: {
            _id, email, firstname, lastname
        }
    });
};

// Map errors of REQUEST input
exports.mapRequestErrors = (req) => {
    var err = validationResult(req);
    if (!err.isEmpty()) {
        err = err.mapped();
        var error = {};
        for (let i in err) {
            error[i] = err[i].msg;
        }
        return {
            success: false,
            error
        };
    } else {
        return {
            success: true
        }
    }
};