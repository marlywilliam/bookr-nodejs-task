const mongoose = require('mongoose');

const ProductSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Please add a product name']
    },
    description: {
        type: String
    },
    stock: {
        type: Number,
        required: [true, 'Please set the available stock']
    },
    price: {
        type: Number,
        required: [true, 'Please set the product price']
    }
});

module.exports = mongoose.model('Product', ProductSchema);