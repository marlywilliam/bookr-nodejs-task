const express = require("express");
const { newProduct, getProducts, buyProduct, userProducts } = require("../controllers/product.controller");
const User = require("../models/user.model");
const Product = require("../models/product.model");
const { body, check } = require("express-validator");
const router = express.Router();
const {protect } = require('../middleware/auth');

router.post(
    "/",
    check(['name', 'stock', 'price'], 'Field is required').notEmpty(),
    check(['stock', 'price']).isNumeric(),
    newProduct
);
router.get("/", getProducts);
router.post(
    "/buy",
    check(['userId', 'product', 'product.id', 'product.quantity'], 'Field is required').notEmpty(),
    check(['product.quantity']).isNumeric(),
    body("userId").custom((userId) => {
        return User.findById(userId).then((user) => {
            if (!user) {
                throw new Error("User you entered does not exist");
            }
        });
    }),
    body('product').custom((product) => {
        return Product.findById(product.id).then((p) => {
            if (!p) {
                throw new Error("Product does not exist");
            } else {
                if (product.quantity == 0) {
                    throw new Error('Quantity must be more than 0');
                }
                if (p.stock < product.quantity) {
                    var msg = "Product out of stock";
                    if (p.stock > 0) msg += `, only ${p.stock} available`;
                    throw new Error(msg);
                }
            }
        });
    }),
    buyProduct
);
router.get("/me", protect, userProducts);

module.exports = router;