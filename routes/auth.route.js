const express = require("express");
const { signup, login } = require("../controllers/auth.controller");
const User = require("../models/user.model");
const { body, check } = require("express-validator");
const router = express.Router();

router.post(
    "/signup",
    check(['firstname', 'lastname', 'password', 'email'], 'Field is required').notEmpty(),
    check("email", 'Enter a valid email').isEmail(),
    body("email").custom((email) => {
        return User.findOne({
            email,
        }).then((user) => {
            if (user) {
                throw new Error("Email already in use");
            }
        });
    }),
    signup
);

router.post(
    "/login",
    check("email", "Enter a valid email").exists().isEmail(),
    check("password", "Enter a password").notEmpty(),
    login
);

module.exports = router;
