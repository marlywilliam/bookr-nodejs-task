# NodeJS Bookr Task

MongoDB is used. The connection URI is in the config file so you can test the code.

## Getting Started

Use the node package manager

```bash
npm install
```

## Usage

Development ENV run ~ runs nodemon:
```python
npm run dev
```

Production ENV run:
```python
npm start
```

All requests is logged and printed in the console by `morgan` in DEV environment.

## APIs
Postman collection and environment are on '/postman' path you can import them.


## 
Thanks for your time

Marly E. William

marlywilliam31@gmail.com