const ErrorResponse = require('../utils/errorResponse');

const errorHandler = (error, req, res, next) => {
    // Mongoose cast error
    if (error.name === 'CastError') {
        const message = `Resource with id ${error.error} cannot be found`;
        error = new ErrorResponse(message, 404);
    }

    // Mongoose duplicate key
    if (error.code === 11000) {
        let returnErrors = {};
        for (let e in error.keyValue) {
            returnErrors[e] = `Field already exists`;
        }
        return res.status(400).json({
            success: false,
            error: returnErrors
        });
    }

    // Mongoose valudation error
    if (error.name === 'ValidationError') {
        let returnErrors = {};
        for (let e in error.errors) {
            returnErrors[e] = error.errors[e].message;
        }
        return res.status(400).json({
            success: false,
            error: returnErrors
        });
    }

    res.status(error.statusCode || 500).json({
        success: false,
        error: error.message || 'Server error'
    })
}

module.exports = errorHandler;