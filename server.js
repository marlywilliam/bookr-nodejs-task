require('colors');
const express = require('express');
const app = express();
const dotenv = require('dotenv');
const connectDB = require('./config/db');
const errorHandler = require('./middleware/error');
const cors = require('cors');
var morgan = require('morgan')

// Load ENV vars
dotenv.config({ path: './config/config.env' });

// Connect to database
connectDB();

// Parse incoming requests with JSON payloads ~ body-parser
app.use(express.json());

// Enable CORS 
app.use(cors());

// Dev logging middleware
if (process.env.NODE_ENV == 'development') {
    app.use(morgan('dev'));
}

// API Routes
app.use('/app/v1/', require('./routes'));

// Other Error Handling
app.use(errorHandler);

const PORT = process.env.PORT || 5000;
const server = app.listen(PORT, console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`.yellow.bold));

// Handle unhandled promise rejections
process.on('unhandledRejection', (err, promise) => {
    console.log(`Error: ${err.message}`.red);

    // Close server and exit process
    // server.close(() => process.exit(1));
});